package com.jain.tavish.bakingapp.Networking;

import com.jain.tavish.bakingapp.ModelClasses.Recipe;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("baking.json")
    Call<List<Recipe>> getRecipe();

}