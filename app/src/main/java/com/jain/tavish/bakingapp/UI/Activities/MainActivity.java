package com.jain.tavish.bakingapp.UI.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jain.tavish.bakingapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
