package com.jain.tavish.bakingapp.Networking;


import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.view.View;

import com.jain.tavish.bakingapp.ModelClasses.Recipe;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkRequest {

    private static Call<List<Recipe>> recipeListCall;
    private static MutableLiveData<List<Recipe>> recipeListLiveData;

    public static MutableLiveData<List<Recipe>> makeNetworkRequest(final View view){

        ApiInterface apiInterface = RetrofitClient.getRetrofitInstance().create(ApiInterface.class);
        recipeListCall = apiInterface.getRecipe();

        recipeListLiveData = new MutableLiveData<>();

        recipeListCall.enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(@NonNull Call<List<Recipe>> call, @NonNull Response<List<Recipe>> response) {
                recipeListLiveData.postValue(response.body());
                view.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<List<Recipe>> call, @NonNull Throwable t) {
                recipeListLiveData = null;
            }
        });

        return recipeListLiveData;

    }

}