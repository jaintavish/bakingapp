package com.jain.tavish.bakingapp.UI.Activities;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jain.tavish.bakingapp.Adapters.StepsAdapter;
import com.jain.tavish.bakingapp.ModelClasses.Ingredients;
import com.jain.tavish.bakingapp.ModelClasses.Steps;
import com.jain.tavish.bakingapp.R;
import com.jain.tavish.bakingapp.UI.Fragments.ViewStepFragment;
import com.jain.tavish.bakingapp.Widget.IngredientWidget;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectStepActivity extends AppCompatActivity {

    private List<Steps> stepsList;
    private boolean twoPane;
    private StepsAdapter stepsAdapter;
    private int position;
    private String recipeName;
    private List<Ingredients> mIngredientsList;
    @BindView(R.id.rv_select_step) RecyclerView selectStepRecyclerView;
    @BindView(R.id.toolbarSelectStep) Toolbar toolbar;
    @BindView(R.id.tv_see_ingredients) TextView tvSeeIngredients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.SelectStepTheme);
        setContentView(R.layout.activity_select_step);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        recipeName = getIntent().getStringExtra("recipeName");
        stepsList = getIntent().getParcelableArrayListExtra("stepsList");
        position = getIntent().getIntExtra("position", -1);
        mIngredientsList = getIntent().getParcelableArrayListExtra("ingredientsList");

        if(position == -1){
            Toast.makeText(this, "Error !!", Toast.LENGTH_SHORT).show();
        }else {

            saveDataToSharedPrefs(recipeName, mIngredientsList);

            getSupportActionBar().setTitle(recipeName);
            selectStepRecyclerView.setHasFixedSize(true);
            selectStepRecyclerView.setLayoutManager(new LinearLayoutManager(this));

            twoPane = false;

            if (findViewById(R.id.view_step_frame_layout) != null) {
                twoPane = true;
                //Show default fragment in tablet
                Bundle arguments = new Bundle();
                Steps step = stepsList.get(0);
                arguments.putParcelable("stepInfo", step);

                ViewStepFragment fragment = new ViewStepFragment();
                fragment.setArguments(arguments);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.view_step_frame_layout, fragment)
                        .commit();
            }


            stepsAdapter = new StepsAdapter(this, stepsList, twoPane);
            selectStepRecyclerView.setAdapter(stepsAdapter);

        }

    }

    public void actionSeeIngredients(View view){
        Intent intent = new Intent(this, SeeIngredientsActivity.class);
        intent.putParcelableArrayListExtra("ingredientsList", getIntent().getParcelableArrayListExtra("ingredientsList"));
        intent.putExtra("recipeName", recipeName);
        startActivity(intent);
    }

    public void saveDataToSharedPrefs(String recipeName, List<Ingredients> ingredientsList){

        Intent intent = new Intent(this, IngredientWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);

        int[] ids = AppWidgetManager.getInstance(getApplication())
                .getAppWidgetI‌​ds(new ComponentName(getApplication(), IngredientWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        sendBroadcast(intent);


        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < ingredientsList.size(); i++) {
            builder.append(ingredientsList.get(i).getIngredient()).append("\n");
        }

        SharedPreferences sharedPref = getSharedPreferences("Recipe", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString( "recipeNameSharedPref", recipeName);
        editor.putInt("positionSharedPref", position);
        editor.putString( "ingredientsStringSharedPref", builder.toString());
        editor.apply();

    }

}
