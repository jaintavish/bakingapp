package com.jain.tavish.bakingapp.UI.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.jain.tavish.bakingapp.Adapters.IngredientsAdapter;
import com.jain.tavish.bakingapp.ModelClasses.Ingredients;
import com.jain.tavish.bakingapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SeeIngredientsActivity extends AppCompatActivity {

    private List<Ingredients> ingredientsList;
    private IngredientsAdapter ingredientsAdapter;
    private String recipeName;
    @BindView(R.id.ingredient_recipe_name) TextView ingredient_recipe_name;
    @BindView(R.id.ingredientsRecyclerView) RecyclerView ingredientsRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.ViewStepTheme);
        setContentView(R.layout.activity_see_ingredients);

        ButterKnife.bind(this);

        ingredientsList = getIntent().getParcelableArrayListExtra("ingredientsList");
        recipeName = getIntent().getStringExtra("recipeName");

        ingredient_recipe_name.setText("Ingredients List for " + recipeName);

        ingredientsRecyclerView.setHasFixedSize(true);
        ingredientsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        ingredientsAdapter = new IngredientsAdapter(this, ingredientsList);
        ingredientsRecyclerView.setAdapter(ingredientsAdapter);

    }
}
