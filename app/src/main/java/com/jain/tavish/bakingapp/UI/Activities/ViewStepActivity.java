package com.jain.tavish.bakingapp.UI.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jain.tavish.bakingapp.R;
import com.jain.tavish.bakingapp.UI.Fragments.ViewStepFragment;

import java.util.Objects;

public class ViewStepActivity extends AppCompatActivity {

    private Bundle arguments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.ViewStepTheme);
        setContentView(R.layout.activity_view_step);

        arguments = new Bundle();
        arguments.putParcelable("stepInfoFromActivity", Objects.requireNonNull(getIntent().getExtras()).getParcelable("stepInfo"));

        ViewStepFragment fragment = new ViewStepFragment();
        fragment.setArguments(arguments);
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.view_step_frame_layout, fragment)
                .commit();

    }
}
