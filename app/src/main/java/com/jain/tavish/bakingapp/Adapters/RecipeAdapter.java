package com.jain.tavish.bakingapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jain.tavish.bakingapp.ModelClasses.Recipe;
import com.jain.tavish.bakingapp.R;
import com.jain.tavish.bakingapp.UI.Activities.SelectStepActivity;
import com.jain.tavish.bakingapp.UI.GlideApp;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder>{

    private final Context mContext;
    private final List<Recipe> mRecipeList;

    public RecipeAdapter(Context context, List<Recipe> recipeList) {
        mContext = context;
        mRecipeList = recipeList;
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.recipe_item_layout, parent, false);
        return new RecipeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecipeViewHolder holder, int position) {

        holder.tv_name_recipe.setText(mRecipeList.get(position).getName());
        holder.tv_servings_count.setText("No. Of Servings : " + mRecipeList.get(position).getServings());
        holder.tv_step_count.setText("No. Of Steps : " + mRecipeList.get(position).getSteps().size());
        holder.tv_ingredients_count.setText("No. Of Ingredients : " + mRecipeList.get(position).getIngredients().size());

        if(TextUtils.isEmpty(mRecipeList.get(position).getImage())){
            GlideApp.with(mContext)
                    .load(R.drawable.ic_cake)
                    .into(holder.iv_recipe);
        }else{
            GlideApp.with(mContext)
                    .load(mRecipeList.get(position).getImage())
                    .error(R.drawable.ic_cake)
                    .into(holder.iv_recipe);
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SelectStepActivity.class);
                intent.putExtra("position", holder.getAdapterPosition());
                intent.putExtra("recipeName", mRecipeList.get(holder.getAdapterPosition()).getName());
                intent.putParcelableArrayListExtra("stepsList", new ArrayList<Parcelable>(mRecipeList.get(holder.getAdapterPosition()).getSteps()));
                intent.putParcelableArrayListExtra("ingredientsList", new ArrayList<Parcelable>(mRecipeList.get(holder.getAdapterPosition()).getIngredients()));

                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mRecipeList == null){
            return 0;
        }else {
            return mRecipeList.size();
        }
    }

    public class RecipeViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.recipe_img) ImageView iv_recipe;
        @BindView(R.id.recipe_name_txt) TextView tv_name_recipe;
        @BindView(R.id.recipe_ingredient_count_txt) TextView tv_ingredients_count;
        @BindView(R.id.recipe_step_count_txt)TextView tv_step_count;
        @BindView(R.id.recipe_servings_count_txt)TextView tv_servings_count;
        @BindView(R.id.ll_main_content) LinearLayout linearLayout;

        RecipeViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }


    }

}
