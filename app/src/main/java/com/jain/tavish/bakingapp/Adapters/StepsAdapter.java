package com.jain.tavish.bakingapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jain.tavish.bakingapp.ModelClasses.Steps;
import com.jain.tavish.bakingapp.R;
import com.jain.tavish.bakingapp.UI.Activities.ViewStepActivity;
import com.jain.tavish.bakingapp.UI.Fragments.ViewStepFragment;
import com.jain.tavish.bakingapp.UI.GlideApp;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StepsAdapter extends RecyclerView.Adapter<StepsAdapter.StepsViewHolder> {

    private final boolean twoPane;
    private final Context context;
    private final List<Steps> stepsList;

    public StepsAdapter(Context mContext, List<Steps> steps, boolean mTwoPane) {
        context = mContext;
        stepsList = steps;
        twoPane = mTwoPane;
    }

    @NonNull
    @Override
    public StepsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.select_step_item_layout, parent, false);
        return new StepsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final StepsViewHolder holder, int position) {
        holder.textView.setText(stepsList.get(position).getShortDescription());

        GlideApp.with(context)
                .load(stepsList.get(position).getThumbnailURL())
                .error(R.drawable.ic_error)
                .into(holder.imageView);

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickEvent(holder);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickEvent(holder);
            }
        });

    }

    private void onClickEvent(StepsViewHolder holder){
        if (twoPane) {
            Bundle arguments = new Bundle();
            arguments.putParcelable("stepInfo", stepsList.get(holder.getAdapterPosition()));
            ViewStepFragment fragment = new ViewStepFragment();
            fragment.setArguments(arguments);
            ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.view_step_frame_layout, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(context, ViewStepActivity.class);
            intent.putExtra("stepInfo", stepsList.get(holder.getAdapterPosition()));
            context.startActivity(intent);
        }
    }

    @Override
    public int getItemCount() {
        return stepsList.size();
    }

    public class StepsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_short_description_item) TextView textView;
        @BindView(R.id.iv_select_step_thumbnail) ImageView imageView;

        StepsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
