package com.jain.tavish.bakingapp.Widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;

import com.jain.tavish.bakingapp.R;
import com.jain.tavish.bakingapp.UI.Activities.MainActivity;

/**
 * Implementation of App Widget functionality.
 */

public class IngredientWidget extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        for (int appWidgetId : appWidgetIds) {

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.ingredient_widget);
            appWidgetManager.updateAppWidget(appWidgetId, views);

            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            views.setOnClickPendingIntent(R.id.btn_show_recipe_widget, pendingIntent);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        String recipeName, ingredientsString;

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.ingredient_widget);

        Intent buttonPressIntent = new Intent(context, MainActivity.class);
        PendingIntent buttonPressPendingIntent = PendingIntent.getActivity(context, 0, buttonPressIntent, 0);
        views.setOnClickPendingIntent(R.id.btn_show_recipe_widget, buttonPressPendingIntent);

        SharedPreferences sharedPref = context.getSharedPreferences("Recipe", Context.MODE_PRIVATE);

        recipeName = sharedPref.getString("recipeNameSharedPref", "");
        ingredientsString = sharedPref.getString("ingredientsStringSharedPref", "");

        views.setTextViewText(R.id.tv_recipe_name_widget, recipeName);
        views.setTextViewText(R.id.tv_ingredients_widget, ingredientsString);

        AppWidgetManager.getInstance(context).updateAppWidget(
               new ComponentName(context, IngredientWidget.class), views);

    }
}