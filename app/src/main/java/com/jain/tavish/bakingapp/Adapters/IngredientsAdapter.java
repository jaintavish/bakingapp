package com.jain.tavish.bakingapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jain.tavish.bakingapp.ModelClasses.Ingredients;
import com.jain.tavish.bakingapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.IngredientsViewHolder>{

    private final Context mContext;
    private final List<Ingredients> mIngredientsList;

    public IngredientsAdapter(Context context, List<Ingredients> ingredientsList) {
        mContext = context;
        mIngredientsList = ingredientsList;
    }

    @NonNull
    @Override
    public IngredientsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.ingredients_row_item, parent, false);
        return new IngredientsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final IngredientsViewHolder holder, final int position) {
        holder.tvIngredientName.setText((position + 1) + ". " + mIngredientsList.get(position).getIngredient());
        holder.tvQuantity.setText("Quantity : " + mIngredientsList.get(position).getQuantity());
        holder.tvMeasure.setText(" " + mIngredientsList.get(position).getMeasure());
    }

    @Override
    public int getItemCount() {
        if (mIngredientsList == null){
            return 0;
        }else {
            return mIngredientsList.size();
        }
    }

    public class IngredientsViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_ingredient_name) TextView tvIngredientName;
        @BindView(R.id.tv_quantity) TextView tvQuantity;
        @BindView(R.id.tv_measure) TextView tvMeasure;

        IngredientsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}