package com.jain.tavish.bakingapp.UI.Fragments;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.jain.tavish.bakingapp.Adapters.RecipeAdapter;
import com.jain.tavish.bakingapp.ModelClasses.Recipe;
import com.jain.tavish.bakingapp.Networking.NetworkRequest;
import com.jain.tavish.bakingapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainFragment extends Fragment {

    @BindView(R.id.rv_recipe) RecyclerView recipeRecyclerView;
    @BindView(R.id.main_progress_bar)ProgressBar progressBar;
    private RecipeAdapter recipeAdapter;
    private Context context;

    public MainFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context =  getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidth = displayMetrics.widthPixels / displayMetrics.density;

        recipeRecyclerView.setHasFixedSize(true);
        if(screenWidth >= 600){
            recipeRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        }else {
            recipeRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        }

        if(NetworkRequest.makeNetworkRequest(progressBar) != null) {
            NetworkRequest.makeNetworkRequest(progressBar).observe((LifecycleOwner) context, new Observer<List<Recipe>>() {
                @Override
                public void onChanged(@Nullable List<Recipe> recipes) {
                    recipeAdapter = new RecipeAdapter(context, recipes);
                    recipeRecyclerView.setAdapter(recipeAdapter);
                }
            });
        }else{
            recipeAdapter = new RecipeAdapter(context, null);
            recipeRecyclerView.setAdapter(recipeAdapter);
        }

        return view;
    }
}