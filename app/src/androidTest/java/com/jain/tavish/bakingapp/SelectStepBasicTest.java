package com.jain.tavish.bakingapp;


import android.os.Handler;
import android.os.Looper;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.jain.tavish.bakingapp.UI.Activities.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;

@RunWith(AndroidJUnit4.class)
public class SelectStepBasicTest {

    @Rule
    public ActivityTestRule<MainActivity> selectStepActivityActivityTestRule =
                        new ActivityTestRule<>(MainActivity.class);

    @Test
    public void clickRecyclerViewItem_opensViewStepActivity(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (Looper.myLooper() == null)
                {
                   Looper.prepare();
                }
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        onData(anything()).inAdapterView(withId(R.id.rv_recipe)).atPosition(0).perform(click());
                        onData(anything()).inAdapterView(withId(R.id.rv_select_step)).atPosition(0).perform(click());
                        onView(withId(R.id.tv_description_view_step)).check(matches(withText("Recipe Introduction")));
                    }
                }, 10000);
            }
        }).run();
    }
}
